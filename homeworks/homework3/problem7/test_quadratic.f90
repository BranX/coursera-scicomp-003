! $MYHPSC/homewors/homework3/fortran/test1.f90

program test_quadratic

    use newton, only: solve, tol
    use functions, only: f_quadratic, fprime_quadratic, epsilon

    implicit none
    real(kind=8) :: x, x0, fx
    real(kind=8) :: x0_vals(2), epsilon_vals(2), tol_vals(2) 
    integer :: i, j, k, run_num, iters
	logical :: debug         ! set to .true. or .false.

    debug = .false.
    run_num = 1
    ! values to test as x0, tol, and epsilon:
    x0_vals = (/-2.d0, 2.d0/)
    epsilon_vals = (/0.5d0, 1.5d0/)
    tol_vals = (/1.d-2, 1.d-14/)

    print *, "Test routine for computing root of function f_quadratic = (x-1)**4 - epsilon"     
    print *, ' '  ! blank line

    do i=1, 2
        do j=1, 2
            epsilon = epsilon_vals(i)        
            tol = tol_vals(j)
                        
            do k=1,2
                x0 = x0_vals(k)

                print 9, run_num
9               format('Run number = ', i3)            

                if (x0>0) then
                    print 10, epsilon, tol, ABS(x0)
10                  format('epsilon = ', e10.5, ',  tol = ', e10.5, ',  x0 = ', e10.5)           
                else
                    print 11, epsilon, tol, ABS(x0)
11                  format('epsilon = ', e10.5, ',  tol = ', e10.5, ',  x0 = -', e10.5)           
                endif
                
                call solve(f_quadratic, fprime_quadratic, x0, x, iters, debug)

                print 12, x, iters
12              format('solver returns x = ', e10.5, ' after', i3, ' iterations')

                fx = f_quadratic(x)
                print 13, fx
13              format('the value of f_quadratic(x) is ', e10.5)
                
                print *, ' '  ! blank line

                run_num = run_num + 1
                enddo
            enddo
        enddo 
end program test_quadratic

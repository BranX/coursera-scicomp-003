! $MYHPSC/homeworks/homework3/fortran/intersections.f90

program intersections

    use newton, only: solve
    use functions, only: h, hprime, f, fprime, g, gprime

    implicit none
    real(kind=8) :: x, x0, hx
    real(kind=8) :: x0vals(4)
    integer :: iters, itest
	logical :: debug         ! set to .true. or .false.

    print *, "Test routine for computing intersection of functions f and g"
    debug = .false.

    ! values to test as x0:
    x0vals = (/0.5d0, 1.5d0, 2.5d0, 3.5d0 /)

    do itest=1,4
        x0 = x0vals(itest)
		print *, ' '  ! blank line
        call solve(h, hprime, x0, x, iters, debug)

        print 11, x, iters
11      format('solver returns x = ', e22.15, ' after', i3, ' iterations')

        hx = h(x)
        print 12, hx
12      format('the value of f(x) - g(x) is ', e22.15)

        enddo

end program intersections
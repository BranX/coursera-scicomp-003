import numpy as np
from newton import solve
import matplotlib.pyplot as plt
from newton import solve


def plot_funcs(f, g):
	x = np.linspace(-10., 10., 10001)
	fx = map(f, x)
	gx = map(g, x)

	plt.figure(1)
	plt.clf()
	plt.plot(x, fx, 'r')
	plt.plot(x, gx, 'b')           
	plt.legend(('Func. f','Func. g'), loc='upper right')
	#plt.show()
	plt.savefig('Intersections.png')


def get_fvals(f, fp, x0):
	return (f(x0), fp(x0))


if __name__ == "__main__":
    f = lambda x: x * np.cos(np.pi*x)
    g = lambda x: 1 - 0.6 * x**2
    h = lambda x: f(x) - g(x)
    
    fp = lambda x: np.cos(np.pi*x) - np.pi*x*np.sin(np.pi*x)
    gp = lambda x: -1.2 * x
    hp = lambda x: fp(x) - gp(x)

    fvals = lambda x: (h(x), hp(x))

    # PLOT FUNCTIONS FIRST
    plot_funcs(f, g)

    # FIND INTERSECTION
    x0 = 0.50
    root, iter_num = solve(fvals, x0, False)
    
    print "Function f and g intersect at %22.15e." % root
    print "Intersection point found after %d iterations." % iter_num
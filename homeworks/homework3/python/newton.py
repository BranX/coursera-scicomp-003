def print_intermediate(x, iter):
	print "After %d iterations, x = %22.15e" % (iter, x)


def solve(fvals, x0, debug=False):
	"""
	:param fvals: function, that returns its 
	              value and its derivative given x
	:param x0: float
	:param debug: boolean
	:returns tuple: float, integer 
	"""

	max_iter = 20
	tol = 10e-14

	root = x0
	iter_num = 1 

	if debug:
		print "Initial guesss: x = %22.15e" % x0

	while iter_num < max_iter:
		(val, der) = fvals(root)
		root = root - val/der 

		if debug:
			print_intermediate(root, iter_num)

		if (abs(fvals(root)[0]) < tol):
			break
		else:
			iter_num = iter_num + 1

	print " "
	return (root, iter_num)


def fvals_sqrt(x):
	"""
	Return  f(x) and f'(x) for applying 
	Newton method to find a square root.
	"""
	f = x**2 - 4
	fp = 2.*x
	return (f, fp)


def test1(debug_solve=False):
	"""
	Test newton iteration for the square root 
	with different initial conditions.
	"""
	import numpy as np 
	for x0 in [1., 2., 100.]:
		(x, iters) = solve(fvals_sqrt, x0, debug=debug_solve)
		print "solve returns x=%22.15e after %i iterations" % (x, iters)
		(fx, fpx) = fvals_sqrt(x)
		print "The value of f(x) is %22.15e" %fx
		assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e" %x

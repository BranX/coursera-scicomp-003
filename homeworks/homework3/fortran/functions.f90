! $MYHPSC/homewors/homework3/fortran/functions.f90

module functions

contains


real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt


! Definition of function f and its prime
real(kind=8) function f(x)
    implicit none
    real(kind=8), parameter :: pi = 3.141592653589793
    real(kind=8), intent(in) :: x

    f = x*cos(pi*x)

end function f


real(kind=8) function fprime(x)
    implicit none
    real(kind=8), parameter :: pi = 3.141592653589793    
    real(kind=8), intent(in) :: x
    
    fprime = cos(pi*x) - pi*x*sin(pi*x)

end function fprime


! Definition of function g and its prime


real(kind=8) function g(x)
    implicit none
    real(kind=8), intent(in) :: x

    g = 1.d0 - 0.6d0*x**2

end function g


real(kind=8) function gprime(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    gprime = -1.2d0 * x

end function gprime

! Definition of function g and its prime
real(kind=8) function h(x)
    implicit none
    real(kind=8), parameter :: pi = 3.141592653589793        
    real(kind=8), intent(in) :: x

    h = f(x) - g(x) 

end function h


real(kind=8) function hprime(x)
    implicit none
    real(kind=8), parameter :: pi = 3.141592653589793    
    real(kind=8), intent(in) :: x

    hprime = fprime(x) - gprime(x)

end function hprime

end module functions


"""
Demonstration module for quadratic interpolation.
Update this docstring to describe your code.
Modified by: ** your name here **
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    # Generation of matrix A
    rows = len(xi)
    #A = np.ones((rows,3))
    #A[:,1]=xi
    #A[:,2]=xi**2
    A = np.vstack([np.ones(3), xi, xi**2]).T
    c = np.linalg.solve(A,yi)
    return c


def cubic_interp(xi,yi):
    """
    Cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 4"
    assert len(xi)==4 and len(yi)==4, error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    # Generation of matrix A
    rows = len(xi)
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T
    c = np.linalg.solve(A,yi)
    return c


def plot_quad(xi, yi):
    c = quad_interp(xi, yi)
    f = lambda x: c[0] + c[1]*x + c[2]*x**2
    x_range = np.linspace(xi.min(), xi.max(), num=100)
    y_range = np.array(map(f,x_range))      
    plt.figure(1)
    plt.clf()
    plt.plot(xi,yi, 'ro')
    #plt.label('original')
    plt.plot(x_range, y_range, 'b--')           
    #plt.label('approximated')
    plt.savefig('OriginalAndApproximated.png')


def plot_cubic(xi, yi):
    c = cubic_interp(xi, yi)
    f = lambda x: c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3
    x_range = np.linspace(xi.min(), xi.max(), num=100)
    y_range = np.array(map(f,x_range))      
    plt.figure(1)
    plt.clf()
    plt.plot(xi,yi, 'ro')
    #plt.label('original')
    plt.plot(x_range, y_range, 'b--')           
    #plt.label('approximated')
    plt.savefig('cubic.png')


def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)


def test_cubic1():
    """
    Test code, no return value or exception if test runs properly.
    """
    pass


def poly_interp(xi, yi):
    """
    Cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have the same length"
    assert len(xi)==len(yi), error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    # Generation of matrix A
    rows = len(xi)
    poly_deg = len(xi) - 1
    cols = []
    cols.append(np.ones(rows))
    for deg in range(1, poly_deg+1):
        cols.append(xi**deg)

    A = np.vstack(cols).T
    c = np.linalg.solve(A,yi)
    return c


def plot_poly(xi, yi):
    c = poly_interp(xi, yi)
    #import pdb
    #pdb.set_trace()
    f = lambda x: np.dot(c,np.power(x, range(len(c))))    
    x_range = np.linspace(xi.min(), xi.max(), num=100)
    y_range = np.array(map(f,x_range))      
    plt.figure(1)
    plt.clf()
    plt.plot(xi,yi, 'ro')
    #plt.label('original')
    plt.plot(x_range, y_range, 'b--')           
    #plt.label('approximated')
    plt.savefig('poly.png')


if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()

